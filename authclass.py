import base64
import requests
import json
from typing import Dict, Any
import webbrowser

class Auth:
    '''base class for both auth classes.'''
    def __init__(self, path: str) -> None:
        self.endpoint = 'https://api.ebay.com/identity/v1/oauth2/token'
        self.authorize = "https://auth.ebay.com/oauth2/authorize"
        self.path = path

        self.get_keys()

        self.clientid = self.keys["clientid"]
        self.clientsecret = self.keys["clientsecret"]
        self.redirect_uri = self.keys["redirect_uri"]
        self.userpass = self.clientid + ":" + self.clientsecret
        self.b64 = base64.b64encode(self.userpass.encode()).decode()

    def get_keys(self) -> None:
        with open(self.path) as f:
            self.keys = json.load(f)

class CCAuth(Auth):
    '''Implements the Client Credentials Grant flow from eBay'''
    def __init__(self, path: str) -> None:
        super().__init__(path)
            
    def auth(self) -> Dict[str, Any]:
        '''Return JSON Client Credentials Application Access Token'''
        self.headers = {
                        "Authorization": "Basic %s" % self.b64,
                        "Content-Type": "application/x-www-form-urlencoded"
                    }
        self.payload = 'grant_type=client_credentials&redirect_uri=Searchez&scope=https://api.ebay.com/oauth/api_scope'

        self.r = requests.post(self.endpoint, headers = self.headers, params = self.payload)
        return self.r.json()

class ACAuth(Auth):
    '''Implements the Authorization code Grant credential flow'''
    def __init__(self, path: str) -> None:
        super().__init__(path)

    def auth(self) -> Dict[str, Any]:
        self.headers = {"Content-Type": "application/x-www-form-urlencoded"}
        self.payload = {
                        "client_id" : self.clientid,
                        "response_type" : "code",
                        "redirect_uri" : self.redirect_uri,
                        "scope" : "https://api.ebay.com/oauth/api_scope",
                        }

        self.r = requests.get(self.authorize, headers = self.headers, params = self.payload)
        webbrowser.open_new(self.r.url)
        self.authorization_response = input("Please enter code: ")

        #Now request access token, refresh token
        self.headers = {
                    "Authorization": "Basic %s" % self.b64,
                    "Content-Type": "application/x-www-form-urlencoded"
                }

        self.payload = r'grant_type=authorization_code' + r'&code=' + self.authorization_response + r'&redirect_uri=' + self.redirect_uri
        self.r = requests.post(self.endpoint, headers=self.headers, params = self.payload)
        return self.r.json()

if __name__ == "__main__":
    #user = CCAuth('secrets.json')
    #val = user.auth()
    #print(val)

    user = ACAuth('secrets.json')
    val = user.auth()
    print(val)