# README

## What/Who is this for?

eBay offers access to its older apis in Python thru [this](https://github.com/timotheus/ebaysdk-python) repo, which is well maintained and offers access to the Trading, Finding, Shopping, and Merchandising APIs. However, these are apparently on the way out; eBay also offers a number of other [APIs](https://developer.ebay.com/docs) that cannot be accessed thru this Python SDK.

In order to access most of the new APIs, you need to authenticate via OAuth2. eBay offers two primary methods of doing so, the [client credentials grant](https://developer.ebay.com/api-docs/static/oauth-client-credentials-grant.html) and the [authorization code grant](https://developer.ebay.com/api-docs/static/oauth-authorization-code-grant.html). This class implements both, and passes  the token and expiration info back as a json object for you to use in later calls to the any of the new APIs. Some APIs will only work with the authorization code grant, so I recommend using it instead of the client credentials grant.

This code is for anyone looking for a (working!) eBay Oauth2 flow in Python3. It's mostly a toy example, and doesn't do much in the way of error handling. Please feel free to modify as you see fit for your application.

## How to use it?

You'll need to setup a [developer account on ebay](https://developer.ebay.com/). From there, you need to create a production/sandbox keyset and note the Client ID and Client Secret Values. Both will need to be pasted in ```secrets.json```. I've included a template called ```secretscopy.json``` that you will need to rename to ```secrets.json``` for things to work.

After that, click on "User Tokens". [It should be to the right of the Client ID box](https://developer.ebay.com/api-docs/static/oauth-redirect-uri.html). You'll need to create an application, which will give you a RuName (also known as the eBay Redirect URL name). Paste this into ```secrets.json``` and you should be ready to go.

For the Client credentials grant, the process is automatic, just run the script. For the authorization code grant, you'll need to sign in via your web browser and copy the ```<code>``` in the URL (eg a successful signin url looks like ```https://signin.ebay.com/ws/eBayISAPI.dll?ThirdPartyAuthSucessFailure&isAuthSuccessful=true&code=<code>&expires_in=299eg```). Paste this into the script, and the code will return a token for use.

Examples for both auth flows are defined in ```authclass.py```

If you have any questions, please ask or open an issue.

## Todo

* implement automatic extraction of code parameter.
* add a demo call of one of the new APIs.
